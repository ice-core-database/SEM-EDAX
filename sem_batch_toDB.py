import os

import psycopg2
from dotenv import dotenv_values
from sqlalchemy import create_engine

import semfile_dbindexing as dbindex
from metadata_dictionaries import core_id_dictionary, instrument_id_dictionary, lab_id_dictionary

# Database Connection
# setting up server connection
#config = dotenv_values('.env')
config = dotenv_values('dev.env')

# os.listdir uses '.' as the current working directory
directory = '/home/hannabrooks/Denali/North_Pacific_Data/Denali/DIC2/Volcanics/SEM/'

filelist = []
for dirpath, dirs, files in os.walk(directory):
    for filename in files:
        if filename.endswith(".pdf"):
            filelist.append(os.path.join(dirpath, filename))

## Ignore OtherGrains folder
filelist = list(filter(lambda file: file.find('OtherGrains') == -1, filelist))
filelist = list(filter(lambda file: file.find('_exclude') == -1, filelist))

## Connect to Database
conn = psycopg2.connect(host=config['DATABASE_HOST'], port=config['DATABASE_PORT'],
                        database=config['DATABASE_NAME'],
                        user=config['DATABASE_USER'], password=config['DATABASE_PASS'])

# create a cursor object
cur = conn.cursor()

# Build data sets (progress bar)
(samples, metadata, chem, inf) = dbindex.generateDataTables(filelist, cur,
                                                            core_id=core_id_dictionary["DEN-13B"],
                                                            lab_id=lab_id_dictionary["Electron Microscopy Laboratory"],
                                                            instrument_id1=instrument_id_dictionary[
                                                                "Vega II XMU tungsten filament scanning electron microscope"],
                                                            instrument_id2=instrument_id_dictionary["Pegasus system"])

(reference_metadata, reference_chem, reference_inf) = dbindex.generateReferenceTables(filelist, cur,
                                                            lab_id=lab_id_dictionary["Electron Microscopy Laboratory"],
                                                            instrument_id1=instrument_id_dictionary[
                                                            "Vega II XMU tungsten filament scanning electron microscope"],
                                                            instrument_id2=instrument_id_dictionary["Pegasus system"])

## Database Disconnect
cur.close()
conn.close()

# Build connection to Database
conn_string = 'postgresql://' + config['DATABASE_USER'] + ':' + config['DATABASE_PASS'] + '@' + \
              config['DATABASE_HOST'] + ':' + config['DATABASE_PORT'] + '/' + config['DATABASE_NAME']
engine = create_engine(conn_string)

# Uploading data to Database
samples.to_sql('sample', schema='denali', con=engine, if_exists='append', index=False)
metadata.to_sql('sem_instrument_metadata', schema='denali', con=engine, if_exists='append', index=False)
chem.to_sql('sem_chemistry', schema='denali', con=engine, if_exists='append', index=False)
inf.to_sql('sem_interference', schema='denali', con=engine, if_exists='append', index=False)

# Uploading references to Database
reference_metadata.to_sql('sem_instrument_metadata', schema='standard_reference_material', con=engine,
                          if_exists='append', index=False)
reference_chem.to_sql('sem_chemistry', schema='standard_reference_material', con=engine, if_exists='append',
                      index=False)
reference_inf.to_sql('sem_interference', schema='standard_reference_material', con=engine, if_exists='append',
                     index=False)
