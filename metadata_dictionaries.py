# Here are the dictionaries for lab, instrument, and core. Set the values as appropriate for the data being imported.
lab_id_dictionary = {
    "ICE Lab": 1,
    "Stable Isotope Lab": 2,
    "Glaciochemistry and Paleoclimate Laboratory": 3,
    "CCI ICP-MS Lab": 4,
    "W.M. Keck Laser Ice Facility": 5,
    "Electron Microscopy Laboratory": 6,
    "Electron Microprobe Laboratory": 7
}

instrument_id_dictionary = {
    "Abakus laser particle detector": 1,
    "conductivity meter": 2,
    "ICS5000 capillary ion chromatograph": 3,
    "L2130i Cavity Ring Down Spectrometer": 4,
    "nondestructive gamma spectrometry counting system": 5,
    "ELEMENT 2 high-resolution sector field ICP-MS": 6,
    "ELEMENT XR high-resolution sector field ICP-MS": 7,
    "UP-213 nm laser": 8,
    "Vega II XMU tungsten filament scanning electron microscope": 9,
    "Pegasus system": 10,
    "SX-100": 11
}

core_id_dictionary = {
    "DEN-13B": 1,
    "DEN-13A": 2,
    "DEN-13C-3": 3,
    "DEN-19": 4,
    "DEN-22": 5
}
