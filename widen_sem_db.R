requiRements::install(path_to_requirements = 'requirements.r.txt')

library(dotenv)
library(DBI)
library(lubridate)
library(tidyr)
library(dplyr)

load_dot_env(file = ".env")

con  <- dbConnect(RPostgres::Postgres(), dbname=Sys.getenv("DATABASE_NAME"), host=Sys.getenv("DATABASE_HOST"),
                 port=Sys.getenv("DATABASE_PORT"), user=Sys.getenv("DATABASE_USER"),
                 password=Sys.getenv("DATABASE_PASS"))

semTable <- dbReadTable(con, "sem")
sem_df <- data.frame(semTable)

dbDisconnect(con)

sem_df$sem_element <- factor(trimws(sem_df$sem_element))
sem_df$sample_name <- factor(trimws(sem_df$sample_name))
sem_df$tube_name <- factor(trimws(sem_df$tube_name))
sem_df$sem_method <- factor(trimws(sem_df$sem_method))
sem_df$quant_method <- factor(trimws(sem_df$quant_method))
sem_df$quant_standard <- factor(trimws(sem_df$quant_standard))
sem_df$sem_user <- factor(trimws(sem_df$sem_user))
sem_df$sem_datetime <- ymd_hms(trimws(sem_df$sem_date))
sem_df$date <- date(sem_df$sem_datetime)

sem_wide <- sem_df %>% select(!c(chem_id, im_id, sem_date)) %>% spread(sem_element, wt_pct)

write.csv(sem_wide, "sem_wide.csv", row.names = FALSE)