import re
from warnings import warn

import pandas as pd
from tqdm import tqdm

import get_semfile


def nextAvailableSampleID(cur):
    '''> This function returns the next available sample ID

    Here's a more detailed description of the function:

    > This function returns the next available sample ID. It does this by querying the database for the maximum sample ID
    and then adding one to it. If the maximum sample ID is None, then the next sample ID is 0

    Parameters
    ----------
    cur
    	cursor to the database

    Returns
    -------
    	The next available sample ID.

    '''
    """
    > This function returns the next available sample ID

    :param cur: cursor to the database
    :return: The next available sample ID.
    """
    # Get indexes to make new rows in database
    cur.execute('SELECT MAX(sample_id) FROM denali.sample')
    next_sample = cur.fetchall()[0][0]
    if (next_sample is None):
        next_sample = 0
    else:
        next_sample += 1
    return next_sample


def nextAvailableSEMIMID(cur, isReference):
    '''> This function returns the next available SEMIMID (SEM Instrument Metadata ID) by querying the database for the maximum
    SEMIMID and adding 1 to it

    Parameters
    ----------
    cur
    	the cursor object

    Returns
    -------
    	The next available SEMIMID

    '''
    if isReference:
        cur.execute('SELECT MAX(sem_im_id) FROM standard_reference_material.sem_instrument_metadata')
    else:
        cur.execute('SELECT MAX(sem_im_id) FROM denali.sem_instrument_metadata')

    next_SEM_IM = cur.fetchall()[0][0]

    if (next_SEM_IM is None):
        next_SEM_IM = 0
    else:
        next_SEM_IM += 1
    return next_SEM_IM


def nextAvailableSEMChemID(cur, isReference):
    '''> This function returns the next available SEMChemID

    Parameters
    ----------
    cur
    	cursor object

    Returns
    -------
    	The next available SEMChemID

    '''

    if isReference:
        cur.execute('SELECT MAX(sem_chem_id) FROM standard_reference_material.sem_chemistry')
    else:
        cur.execute('SELECT MAX(sem_chem_id) FROM denali.sem_chemistry')

    next_SEM_Chem = cur.fetchall()[0][0]

    if (next_SEM_Chem is None):
        next_SEM_Chem = 0
    else:
        next_SEM_Chem += 1
    return next_SEM_Chem


def nextAvailableSEMIFRID(cur, isReference):
    '''> It returns the next available SEM_IFR_ID

    Parameters
    ----------
    cur
    	cursor to the database

    Returns
    -------
    	The next available SEM_IFR_ID

    '''
    if isReference:
        cur.execute('SELECT MAX(sem_ifr_id) FROM standard_reference_material.sem_interference')
    else:
        cur.execute('SELECT MAX(sem_ifr_id) FROM denali.sem_interference')

    next_SEM_IFR = cur.fetchall()[0][0]

    if (next_SEM_IFR is None):
        next_SEM_IFR = 0
    else:
        next_SEM_IFR += 1
    return next_SEM_IFR


def metadataTube(metadata, core_id, cur):
    '''> If the tubemeter is not None, then find the tube_id of the tube with the same name as the tubemeter. If the tubemeter
    is None, then find the tube_id of the tube with the highest tube_id

    Parameters
    ----------
    metadata
    	a dictionary of metadata
    core_id
    	the core id of the core we're currently working on
    cur
    	the cursor object

    Returns
    -------
    	The next tube ID

    '''
    if (metadata['tubemeter'][0] is None):
        cur.execute('SELECT MAX(tube_id) FROM denali.tube')
        next_tubeID = cur.fetchall()[0][0]
    else:
        tubeName = "{:<50}".format(int(metadata['tubemeter'][0]))
        cur.execute("""SELECT tube_id FROM denali.tube WHERE core_id = %(core_id)s AND tube_name = %(name)s ;""",
                    {"name": tubeName, "core_id": core_id})
        next_tubeID = cur.fetchall()[0][0]

    if (next_tubeID is None):
        warn('No match for tube found')

    return next_tubeID


def generateSample(metadata, next_sample, next_tubeID):
    '''It takes the metadata and the next sample and tube IDs, and returns a dataframe with the sample ID, core ID, tube ID,
    and sample name

    Parameters
    ----------
    metadata
    	a dataframe with the metadata for the samples
    next_sample
    	the next sample number to use
    next_tubeID
    	the next tube ID to be used. This is incremented by 1 for each sample.

    Returns
    -------
    	A dataframe with the sample_id, core_id, tube_id, and sample_name

    '''
    samplename = str(metadata['particle'][0])
    if (metadata['puck'][0] is not None):
        samplename = str(metadata['particle'][0]) + '-' + str(metadata['puck'][0])

    return pd.DataFrame(
        {'sample_id': [next_sample], 'core_id': [1], 'tube_id': [next_tubeID], 'sample_name': [str(samplename)]})


def generateMetadata(metadata):
    metadata_part = pd.DataFrame({
        'sem_im_id': [int(metadata['sem_im_id'][0])],
        'sample_id': [int(metadata['sample_id'][0])],
        'lab_id': [int(metadata['lab_id'][0])],
        'instrument_id1': [int(metadata['instrument_id1'][0])],
        'instrument_id2': [int(metadata['instrument_id2'][0])],
        'method': [metadata['method'][0]],
        'label': [metadata['label'][0]],
        'nrm_percent': [metadata['NrmPercent'][0]],
        'kv': [metadata['kv'][0]],
        'tilt': [metadata['tilt'][0]],
        'takeoff': [metadata['takeoff'][0]],
        'detector_type': [metadata['detectorType'][0]],
        'res': [metadata['res'][0]],
        'ampt': [metadata['ampt'][0]],
        'lsec': [metadata['lsec'][0]],
        'fs': [metadata['fs'][0]],
        'date': [str(metadata['date'][0])],
        'quantification_method': [metadata['quantificationMethod'][0]],
        'quantification_standard': [metadata['quantificationStandard'][0]],
        'quantification_type': [metadata['quantificationType'][0]],
        'sem_user': [metadata['SECUser'][0]]})
    return metadata_part


def generateChemistryTable(df):
    '''This function takes a dataframe as input and returns a dataframe with the columns: sem_chem_id, sem_im_id, sample_id,
    element, wt_percent, mol_percent, z, a, f

    Parameters
    ----------
    df
    	the dataframe that contains the data

    Returns
    -------
    	A dataframe with the columns: sem_chem_id, sem_im_id, sample_id, element, wt_percent, mol_percent, z, a, f

    '''
    chem_df = df[['sem_chem_id', 'sem_im_id', 'sample_id', 'Element', 'WtPercent', 'MolPercent', 'Z', 'A', 'F']]
    chem_df = chem_df.rename(columns={'Element': 'element', 'WtPercent': 'wt_percent', 'MolPercent': 'mol_percent',
                                      'Z': 'z', 'A': 'a', 'F': 'f'})
    return chem_df


def uploadChemistryTable(tableName, df, engine):
    '''This function takes in a dataframe, and returns a dataframe with the following columns:

    - `sample_id`: the sample id
    - `chem_id`: the chemical id
    - `chem_name`: the chemical name
    - `chem_cas`: the chemical cas number
    - `chem_type`: the chemical type
    - `chem_amount`: the amount of the chemical in the sample
    - `chem_unit`: the unit of the chemical amount
    - `chem_location`: the location of the chemical in the sample
    - `chem_location_type`: the type of the chemical location
    - `chem_location_unit`: the unit of the chemical location
    - `chem_location_basis`: the basis of the chemical location
    - `chem_location_value`: the value of the chemical location
    - `chem_location_upper`: the

    Parameters
    ----------
    tableName
    	the name of the table you want to create in the database
    df
    	the dataframe that you want to upload
    engine
    	the sqlalchemy engine object

    '''
    chem_df = generateChemistryTable(df)
    chem_df.to_sql(tableName, con=engine, if_exists='append', index=False)


def generateInterferenceTable(df):
    '''This function takes a dataframe as input and returns a dataframe with the columns 'sem_ifr_id', 'sem_im_id',
    'sample_id', 'element', 'net_interference', 'background_interference', and 'p_b'

    Parameters
    ----------
    df
    	the dataframe containing the data

    Returns
    -------
    	A dataframe with the columns: sem_ifr_id, sem_im_id, sample_id, element, net_interference, background_interference, p_b

    '''
    inf_df = df[
        ['sem_ifr_id', 'sem_im_id', 'sample_id', 'Element', 'Net_Interference', 'Interference_Error',
         'Background_Interference', 'P_B']]
    inf_df = inf_df.rename(columns={'Element': 'element', 'Net_Interference': 'net_interference',
                                    'Interference_Error': 'interference_error',
                                    'Background_Interference': 'background_interference', 'P_B': 'p_b'})
    return inf_df


def uploadInterferenceTable(tableName, df, engine):
    '''It takes a dataframe, and returns a dataframe with the same columns, but with the values in the columns replaced with
    the number of times that value appears in the column

    Parameters
    ----------
    tableName
    	the name of the table you want to create in the database
    df
    	the dataframe that contains the data you want to upload
    engine
    	the sqlalchemy engine object

    '''
    inf_df = generateInterferenceTable(df)
    inf_df.to_sql(tableName, con=engine, if_exists='append', index=False)


def generateDataTables(fileList, cur, core_id, lab_id, instrument_id1, instrument_id2):
    '''It takes a list of PDF files, extracts the data from each file, and then builds a dataframe for each table in the
    database

    Parameters
    ----------
    fileList
    	a list of file paths to the PDFs you want to extract data from
    cur
    	cursor object for the database
    core_id
    	the core ID of the core you're working with
    lab_id
    	the lab that the instrument is in
    instrument_id1
    	the id of the instrument used to collect the data
    instrument_id2
    	the instrument ID for the SEM

    Returns
    -------
    	A tuple of dataframes

    '''
    ## prepare empty data tables
    samples_main = pd.DataFrame({'sample_id': [],
                                 'core_id': [],
                                 'tube_id': [],
                                 'sample_name': []
                                 })

    metadata_main = pd.DataFrame({
        'sem_im_id': [],
        'sample_id': [],
        'method': [],
        'label': [],
        'nrm_percent': [],
        'kv': [],
        'tilt': [],
        'takeoff': [],
        'detector_type': [],
        'res': [],
        'ampt': [],
        'lsec': [],
        'fs': [],
        'date': [],
        'quantification_method': [],
        'quantification_standard': [],
        'quantification_type': [],
        'sem_user': [],
        'instrument_id1': [],
        'instrument_id2': [],
        'lab_id': []
    })

    chem_main = pd.DataFrame({
        'sem_chem_id': [],
        'sem_im_id': [],
        'sample_id': [],
        'element': [],
        'wt_percent': [],
        'mol_percent': [],
        'z': [],
        'a': [],
        'f': []
    })

    inf_main = pd.DataFrame({
        'sem_ifr_id': [],
        'sem_im_id': [],
        'sample_id': [],
        'element': [],
        'net_interference': [],
        'interference_error': [],
        'background_interference': [],
        'p_b': []
    })

    next_sample = nextAvailableSampleID(cur)
    next_SEM_IM = nextAvailableSEMIMID(cur, isReference=False)
    next_SEM_Chem = nextAvailableSEMChemID(cur, isReference=False)
    next_SEM_IFR = nextAvailableSEMIFRID(cur, isReference=False)

    ## grab data from each file and build larger data set.
    for file in tqdm(fileList):
        # Call function to extract data from PDF
        file_data = get_semfile.get_semfile(file, False)
        # Grab metadata from extracted data
        if 'reference' not in file_data[0]['method'][0]:
            metadata = file_data[0]

            # Match tube to database tubes
            next_tubeID = metadataTube(metadata, cur=cur, core_id=core_id)

            ##to identify max IDs
            metadata['sample_id'] = next_sample
            metadata['sem_im_id'] = next_SEM_IM
            metadata['instrument_id1'] = instrument_id1
            metadata['instrument_id2'] = instrument_id2
            metadata['lab_id'] = lab_id

            SEMChemistry = file_data[1]
            SEMChemistry['sample_id'] = next_sample
            SEMChemistry['sem_im_id'] = next_SEM_IM
            SEMChemistry['sem_chem_id'] = range(next_SEM_Chem, next_SEM_Chem + SEMChemistry.shape[0], 1)

            SEMInterference = file_data[2]
            SEMInterference['sample_id'] = next_sample
            SEMInterference['sem_im_id'] = next_SEM_IM
            SEMInterference['sem_ifr_id'] = range(next_SEM_IFR, next_SEM_IFR + SEMInterference.shape[0], 1)

            samples_main = pd.concat((samples_main, generateSample(metadata, next_sample, next_tubeID)),
                                     ignore_index=True)
            metadata_main = pd.concat((metadata_main, generateMetadata(metadata)), ignore_index=True)
            chem_main = pd.concat((chem_main, generateChemistryTable(SEMChemistry)), ignore_index=True)
            inf_main = pd.concat((inf_main, generateInterferenceTable(SEMInterference)), ignore_index=True)

            ## increment values for next file
            next_sample += 1
            next_SEM_IM += 1
            next_SEM_Chem += (SEMChemistry.shape[0] + 1)
            next_SEM_IFR += (SEMInterference.shape[0] + 1)
    return (samples_main, metadata_main, chem_main, inf_main)


### --- REFERENCES --- ###
def generateReferenceTables(fileList, cur, lab_id, instrument_id1, instrument_id2):
    ## prepare empty data tables

    metadata_main = pd.DataFrame({
        'sem_im_id': [],
        'std_ref_id': [],  # added this to the table
        'method': [],
        'ref_dt': [],
        'label': [],
        'nrm_percent': [],
        'kv': [],
        'tilt': [],
        'takeoff': [],
        'detector_type': [],
        'res': [],
        'ampt': [],
        'lsec': [],
        'fs': [],
        'date': [],
        'quantification_method': [],
        'quantification_standard': [],
        'quantification_type': [],
        'sem_user': [],
        'instrument_id1': [],
        'instrument_id2': [],
        'lab_id': []
    })

    chem_main = pd.DataFrame({
        'sem_chem_id': [],
        'sem_im_id': [],
        'std_ref_id': [],  # added this to the table
        'element': [],
        'wt_percent': [],
        'mol_percent': [],
        'z': [],
        'a': [],
        'f': []
    })

    inf_main = pd.DataFrame({
        'sem_ifr_id': [],
        'sem_im_id': [],
        'std_ref_id': [],  # added this to the table
        'element': [],
        'net_interference': [],
        'interference_error': [],
        'background_interference': [],
        'p_b': []
    })

    next_SEM_IM = nextAvailableSEMIMID(cur, isReference=True)
    next_SEM_Chem = nextAvailableSEMChemID(cur, isReference=True)
    next_SEM_IFR = nextAvailableSEMIFRID(cur, isReference=True)

    # grab data from each file and build larger data set.
    for file in tqdm(fileList):
        # Call function to extract data from PDF
        file_data = get_semfile.get_semfile(file, False)
        # Grab metadata from extracted data
        if 'reference' in file_data[0]['method'][0]:
            metadata = file_data[0]
            std_ref_id = parseReferenceID(string=metadata['method'][0], cur=cur)
            # match metadata to new reference table primary key
            metadata['std_ref_id'] = std_ref_id

            # to identify max IDs
            metadata['sem_im_id'] = next_SEM_IM
            metadata['instrument_id1'] = instrument_id1
            metadata['instrument_id2'] = instrument_id2
            metadata['lab_id'] = lab_id

            SEMChemistry = file_data[1]
            SEMChemistry['sem_im_id'] = next_SEM_IM
            SEMChemistry['sem_chem_id'] = range(next_SEM_Chem, next_SEM_Chem + SEMChemistry.shape[0], 1)
            SEMChemistry['std_ref_id'] = std_ref_id


            SEMInterference = file_data[2]
            SEMInterference['sem_im_id'] = next_SEM_IM
            SEMInterference['sem_ifr_id'] = range(next_SEM_IFR, next_SEM_IFR + SEMInterference.shape[0], 1)
            SEMInterference['std_ref_id'] = std_ref_id


            # here make copies of generateMetadata, generateChemistryTable, generateInterferencceTable for reference - as appropriate
            metadata_main = pd.concat((metadata_main, generateReferenceMetadata(metadata)), ignore_index=True)
            chem_main = pd.concat((chem_main, generateReferenceChemistryTable(SEMChemistry)), ignore_index=True)
            inf_main = pd.concat((inf_main, generateReferenceInterferenceTable(SEMInterference)), ignore_index=True)

            ## increment values for next file
            next_SEM_IM += 1
            next_SEM_Chem += (SEMChemistry.shape[0] + 1)
            next_SEM_IFR += (SEMInterference.shape[0] + 1)

    return (metadata_main, chem_main, inf_main)


def generateReferenceMetadata(metadata):
    metadata_part = pd.DataFrame({
        'sem_im_id': [int(metadata['sem_im_id'][0])],
        'std_ref_id': [int(metadata['std_ref_id'][0])],  # changed sample_id to std_ref_id
        'lab_id': [int(metadata['lab_id'][0])],
        'instrument_id1': [int(metadata['instrument_id1'][0])],
        'instrument_id2': [int(metadata['instrument_id2'][0])],
        'method': [metadata['method'][0]],
        'ref_dt': [metadata['ref_dt'][0]],
        'label': [metadata['label'][0]],
        'nrm_percent': [metadata['NrmPercent'][0]],
        'kv': [metadata['kv'][0]],
        'tilt': [metadata['tilt'][0]],
        'takeoff': [metadata['takeoff'][0]],
        'detector_type': [metadata['detectorType'][0]],
        'res': [metadata['res'][0]],
        'ampt': [metadata['ampt'][0]],
        'lsec': [metadata['lsec'][0]],
        'fs': [metadata['fs'][0]],
        'date': [str(metadata['date'][0])],
        'quantification_method': [metadata['quantificationMethod'][0]],
        'quantification_standard': [metadata['quantificationStandard'][0]],
        'quantification_type': [metadata['quantificationType'][0]],
        'sem_user': [metadata['SECUser'][0]]})
    return metadata_part


def parseReferenceID(string: str, cur) -> int:
    """
    Take metadata from file_data and transform it into a primary key from the referencce published metadata database table
    Parameters
    ----------
    :string: a string from method in file_data
    :type string: str
    :return: std_ref_id primary key index from standard_reference_material.std_ref_published_metadata
    :return type: int
    """
    # regex this string
    mount_id = re.search(pattern='(?<=_)[\w|-]+', string=string).group()

    # map to database table primary key
    return getRefID(cur, mount_id)


def getRefID(cur, mount_id):
    query = "SELECT std_ref_id FROM standard_reference_material.std_ref_published_metadata WHERE standard_reference_material.std_ref_published_metadata.mount_id_number LIKE '" + mount_id + "%' LIMIT 1"

    cur.execute(query)

    RefID_result = cur.fetchone()

    if (RefID_result is None):
        raise ValueError("MOUNT ID: '" + str(mount_id) + "' Found no match in database")
        return None
    else:
        return RefID_result[0]


def generateReferenceChemistryTable(df):
    chem_df = df[['sem_chem_id', 'sem_im_id', 'std_ref_id', 'Element', 'WtPercent', 'MolPercent', 'Z', 'A',
                  'F']]  # changed sample_id to std_ref_id
    chem_df = chem_df.rename(columns={'Element': 'element', 'WtPercent': 'wt_percent', 'MolPercent': 'mol_percent',
                                      'Z': 'z', 'A': 'a', 'F': 'f'})
    return chem_df


def generateReferenceInterferenceTable(df):
    inf_df = df[['sem_ifr_id', 'sem_im_id', 'std_ref_id', 'Element', 'Net_Interference', 'Interference_Error',
                 'Background_Interference',
                 'P_B']]  # changed sample_id to std_ref_id
    inf_df = inf_df.rename(columns={'Element': 'element', 'Net_Interference': 'net_interference',
                                    'Interference_Error': 'interference_error',
                                    'Background_Interference': 'background_interference', 'P_B': 'p_b'})
    return inf_df
