# dependencies: PyMuPDF (fitz), Pillow (Image, ImageTk), DateUtil (dateparser)
import os.path
import re  # regex
import warnings

import dateutil.parser as dateparser
import fitz  # Install PyMuPDF with pip, version in conda is unmaintained
import hyperspy.api as hs
import pandas as pd
from pandas import DataFrame
from PIL import Image


def get_semfile(filename: str, export: bool) -> tuple:
    """
    It takes a filename, and returns a tuple of dataframes containing the metadata, chemistry, interference, and spectrum data

    :param filename: the path to the .pdf file
    :type filename: str
    :param export: bool = True
    :type export: bool
    :return: A tuple of dataframes
    """
    doc = fitz.open(filename)

    filedir = os.path.dirname(filename)

    filedir_short = re.search('(?<=\/SEM\/)\w*', filedir).group(0)
    #directory metadata

    # find metadata from filename
    name = re.search('.*(?=\.pdf)', os.path.basename(filename)).group(0)

    # This section is specific to my nomenclature
    # replace this section with your own file naming system, and also fieldnames in the metadata block for CSV export

    filename_meta = {'tubemeter': None, 'particle': None, 'puck': None, 'method': None, 'ref_dt': None}

    # polished epoxy pucks: tubemeter, particle
    polished_data = re.search('(?P<tubemeter>\d{2,3})_(?P<particle>\w\d{1,2}(\-\d)?)', filename)
    if polished_data is not None:
        tubemeter = float(polished_data.group('tubemeter'))
        particle = polished_data.group('particle')
        method = 'polished'
        filename_meta = {'tubemeter': tubemeter, 'particle': particle, 'puck': None, 'method': method, 'ref_dt': None}

    # Unmounted, unpolished filter samples regex: tubmeter, puck, particle
    unpolished_data = re.search('(?P<tubemeter>\d{2,3})_(?P<puck>\d{1,2})\-(?P<particle>\d{1,3}\w*)', filename)
    if unpolished_data is not None:
        tubemeter = float(unpolished_data.group('tubemeter'))
        puck = float(unpolished_data.group('puck'))
        particle = unpolished_data.group('particle')
        method = 'unpolished'
        filename_meta = {'tubemeter': tubemeter, 'particle': particle, 'puck': puck, 'method': method, 'ref_dt': None}

    # reference material names
    # BHVO-2G, PM2-15, PM2-16, ATHO-G --> regex: reference, day, month
    reference_data = re.search('(?P<reference>[\w|-]+)_(?P<day>\d\d)(?P<month>\w{2,4})(?P<year>\d{4})(?=.pdf)',
                               filename)
    if reference_data is not None:
        method = 'reference_' + reference_data.group('reference')
        ref_day = reference_data.group('day')
        ref_month = reference_data.group('month')
        ref_year = reference_data.group('year')
        ref_dt = dateparser.parse(ref_day + '-' + ref_month + '-' + ref_year)
        filename_meta = {'tubemeter': None, 'particle': None, 'puck': None, 'method': method, 'ref_dt': ref_dt}

    if (polished_data is None) & (unpolished_data is None) & (reference_data is None):
        warnings.warn('No filename structure detected in: ' + filename)

    # This begins the TEXT EXTRACTION and IMAGE EXTRACTION
    page0 = doc.load_page(0)

    # extract all of the text from the document
    text0 = page0.get_text()

    if doc.page_count == 2:
        page1 = doc.load_page(1)
        text0 = text0 + '\n' + page1.get_text()

    if doc.page_count == 2:
        # adjust this to change the amount that the pixels
        pixdensity = 3
        mat = fitz.Matrix(pixdensity, pixdensity)
        pix = page0.get_pixmap(matrix=mat)
        # print(pix) #print pixel dimensions of pixmap and color scheme

        mode = "RGBA" if pix.alpha else "RGB"
        img = Image.frombytes(mode, [pix.width, pix.height], pix.samples)

        # crop to spectra plot using hardcoded dimensions
        img = img.crop((74 * pixdensity, 149 * pixdensity, 545 * pixdensity, 473 * pixdensity))
        # Export .png
        if export:
            img.save(os.path.join(name + '.spectra.png'))
            # img.show()

    # find metadata with regex out of extracted text
    label = str(re.search('((?<=Label:)|(?<=Label\s:\s))\w*', text0).group(0))
    NrmPercent = re.search('(?<=Nrm\.\%\=\s).*(?=\))', text0).group(0)
    kv = float(re.search('((?<=kV:)|(?<=kV\s:\s))\d*\.\d*', text0).group(0))
    tilt = float(re.search('((?<=Tilt:)|(?<=Tilt:\s))\d*\.\d*', text0).group(0))
    takeoff = float(re.search('(?<=Take-off:)\d*\.\d*', text0).group(0))
    detectorType = re.search('((?<=Detector\sType(:))|(?<=Det\sType))\w*\s\w*\s\d*', text0).group(0)
    res = float(re.search('((?<=Res:)|(?<=Resolution:))\d*(\.\d*)?', text0).group(0))
    ampt = float(re.search('((?<=Amp\.T:)|(?<=AmpT\s:\s))\d*(\.\d*)?', text0).group(0))
    lsec = float(re.search('((?<=Lsec\s)|(?<=Lsec\s:\s))\d{1,4}', text0).group(0))
    if re.search('(?<=FS\s:\s)\d*', text0) is not None:
        fs = float(re.search('(?<=FS\s:\s)\d*', text0).group(0))
    else:
        fs = None

    date = re.search('\d(\d)?\-\w*\-\d{4}', text0).group(0)
    time = re.search('\d\d\:\d\d\:\d\d', text0).group(0)

    dt = dateparser.parse(date + ' ' + time)

    # table 1 metadata
    quantificationMethod = re.search('(?<=EDAX\s).*Quantification', text0).group(0)
    quantificationStandard = re.search('(?<=Quantification\s\().*(?=\))', text0).group(
        0)  # assumes that the standard is printed as a paranthetical
    quantificationType = re.search('(?<=\)\n).*(?=\nSEC)', text0).group(0)
    SECUser = re.search('(?<=SEC\sTable\s:\s).*', text0).group(0)

    metadata = {'filename' : name,
                'project' : filedir_short,
                'filedir' : filedir,
                'method': filename_meta['method'],
                'tubemeter': filename_meta['tubemeter'],
                'particle': filename_meta['particle'],
                'puck': filename_meta['puck'],
                'ref_dt': filename_meta['ref_dt'],
                'label': label,
                'NrmPercent': NrmPercent,
                'kv': kv,
                'tilt': tilt,
                'takeoff': takeoff,
                'detectorType': detectorType,
                'res': res,
                'ampt': ampt,
                'lsec': lsec,
                'fs': fs,
                'date': dt,
                'quantificationMethod': quantificationMethod,
                'quantificationStandard': quantificationStandard,
                'quantificationType': quantificationType,
                'SECUser': SECUser}

    metadf = pd.DataFrame(data=metadata, index=[0])
    metadf['filename'] = metadf['filename'].astype('string')
    metadf['filedir'] = metadf['filedir'].astype('string')
    metadf['project'] = metadf['project'].astype('string')
    metadf['method'] = metadf['method'].astype('string')
    metadf['particle'] = metadf['particle'].astype('string')
    metadf['label'] = metadf['label'].astype('string')
    metadf['detectorType'] = metadf['detectorType'].astype('string')
    metadf['quantificationMethod'] = metadf['quantificationMethod'].astype('string')
    metadf['quantificationStandard'] = metadf['quantificationStandard'].astype('string')
    metadf['quantificationType'] = metadf['quantificationType'].astype('string')
    metadf['SECUser'] = metadf['SECUser'].astype('string')

    # extract data for Table0
    # assume table zero has the following columns:
    # Element, Wt%, Mol%, Z, A, F

    # lists for each variable
    table0Element = []
    table0WtPercent = []
    table0MolPercent = []
    table0Z = []
    table0A = []
    table0F = []

    table0RowSearch = re.finditer(
        '[ ]{1,5}(?P<Element>\w*)[ ]{1,5}(?P<WtPct>\d{1,4}\.\d{1,4})[ ]{1,5}(?P<MolPct>\d{1,4}\.\d{1,4})[ ]{1,5}(?P<Z>\d{1,4}\.\d{1,4})[ ]{1,5}(?P<A>\d{1,4}\.\d{1,4})[ ]{1,5}(?P<F>\d{1,4}\.\d{1,4})',
        text0)

    for match in table0RowSearch:
        table0Element.append(str(match.group('Element')))
        table0WtPercent.append(float(match.group('WtPct')))
        table0MolPercent.append(float(match.group('MolPct')))
        table0Z.append(float(match.group('Z')))
        table0A.append(float(match.group('A')))
        table0F.append(float(match.group('F')))

    table0RowTotal = re.search(
        '[ ]{1,5}(?P<Element>Total)[ ]{1,5}(?P<WtPct>\d{1,4}\.\d{1,4})[ ]{1,5}(?P<MolPct>\d{1,4}\.\d{1,4})', text0)

    table0Element.append(str(table0RowTotal.group('Element')))
    table0WtPercent.append(float(table0RowTotal.group('WtPct')))
    table0MolPercent.append(float(table0RowTotal.group('MolPct')))
    table0Z.append(None)
    table0A.append(None)
    table0F.append(None)

    # make into data frame with PANDAS
    table0data = {'Element': table0Element,
                  'WtPercent': table0WtPercent,
                  'MolPercent': table0MolPercent,
                  'Z': table0Z,
                  'A': table0A,
                  'F': table0F}

    table0df = pd.DataFrame(data=table0data)
    table0df['Element'] = table0df['Element'].astype('string')

    # extract data for Table1
    # assume table zero has the following columns:
    # Element, Net Interference, Background Interference, Interference Error, P/B

    # lists for each variable
    table1Element = []
    table1NetInter = []
    table1BkgdInter = []
    table1InterError = []
    table1PB = []

    table1RowSearch = re.finditer(
        '[ ]{1,5}(?P<Element>\w*(\s\w*)?)[ ]{1,10}(?P<NetInter>\d{1,4}\.\d{1,4})[ ]{1,10}(?P<BkgdInter>\d{1,4}\.\d{1,4})[ ]{1,10}(?P<InterError>\d{1,4}\.\d{1,4})[ ]{1,10}(?P<PB>\d{1,4}\.\d{1,4})[ ]\n',
        text0)

    for match in table1RowSearch:
        table1Element.append(str(match.group('Element')))
        table1NetInter.append(float(match.group('NetInter')))
        table1BkgdInter.append(float(match.group('BkgdInter')))
        table1InterError.append(float(match.group('InterError')))
        table1PB.append(float(match.group('PB')))

    # make into data frame with PANDAS
    table1data = {'Element': table1Element,
                  'Net_Interference': table1NetInter,
                  'Background_Interference': table1BkgdInter,
                  'Interference_Error': table1BkgdInter,
                  'P_B': table1PB}

    table1df = pd.DataFrame(data=table1data)
    table1df['Element'] = table1df['Element'].astype('string')

    # Extract data from spc file
    spcdf = None
    if os.path.exists(os.path.join(filedir,name + '.spc')):
        if hs.load(os.path.join(filedir,name + '.spc')) is not None:
            spc = hs.load(os.path.join(filedir,name + '.spc'))

            total_dataPoints = spc.original_metadata.spc_header.numPts
            step = spc.original_metadata.spc_header.evPerChan

            spcdf = pd.DataFrame(data={'spc': spc.data})
            spcdf = spcdf.convert_dtypes()

    if export:
        # Exports
        metadf.to_csv(os.path.join(filedir, name + '.metadata.csv'))
        # Export .csv
        table0df.to_csv(os.path.join(filedir, name + '.table0.csv'), index=False)
        # Export .csv
        table1df.to_csv(os.path.join(filedir, name + '.table1.csv'), index=False)
        # Export .csv
        spcdf.to_csv(os.path.join(filedir, name + '.spectrumtable.csv'), index=True)

    # outputs
    extracted_dataframes: tuple[DataFrame, DataFrame, DataFrame, DataFrame | None] = (metadf, table0df, table1df, spcdf)
    return extracted_dataframes